package com.twuc.webApp.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
@ControllerAdvice
public class DemoController {

    //2.1
    @GetMapping("/api/errors/default")
    public ResponseEntity<String> getError() {
        throw new RuntimeException();
    }

    //2.2
    @GetMapping("/api/exceptions/access-control-exception")
    public ResponseEntity<String> getAccessControlException() {
        throw new AccessControlException("");
    }

    @ExceptionHandler
    public ResponseEntity<String> handle(RuntimeException exception) {
        return ResponseEntity.status(404).body("handle " + exception.getClass().getSimpleName());
    }

    @ExceptionHandler
    public ResponseEntity<String> getErrorMessage(AccessControlException exception) {
        return ResponseEntity.status(403).body("handle new " + exception.getClass().getSimpleName());
    }

    //2.3
    @GetMapping("/api/errors/null-pointer")
    public ResponseEntity getNullPointerException() {
        throw new NullPointerException();
    }

    //2.3
    @GetMapping("/api/errors/arithmetic")
    public ResponseEntity getArithmeticException() {
        throw new ArithmeticException();
    }

    //2.3
    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity handleWithOneResult(Exception e) {
        return ResponseEntity.status(418).body("Something wrong with the argument");
    }

    //2.4
    @GetMapping("/api/sister-errors/illegal-argument")
    public ResponseEntity getHandleException() {
        throw new IllegalArgumentException();
    }

    //2.4
    @ExceptionHandler
    public ResponseEntity handleWithOneException(IllegalArgumentException e) {
        return ResponseEntity.status(418).body("Something wrong with brother or sister.");
    }
}
